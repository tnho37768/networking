## Extended DHCP configuration with nat and special security rules.
/* Troels Hoest */
version 12.1X47-D15.4;
system {
    host-name srx2;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/"; ## Rootpass
    }
    services {
        ssh;
        dhcp-local-server {
            group internet {
                interface ge-0/0/0.0;
            }
            group IoT-Net1 {
                interface ge-0/0/1.0;
            }
            group IoT-Net2 {
                interface ge-0/0/2.0;
            }
        }
    }
}
interfaces {
    ge-0/0/0 {
        unit 0 {
            family inet {
                address 10.56.16.2/22
            }
        }
    }
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 192.168.11.1/26;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.12.1/28;
            }
        }
    }
}
routing-options {
    static {
        route 0.0.0.0/0 next-hop 10.56.16.1;
    }
}
security {
    nat {
        /* NAT changes the source address of egress IP packets */
        source {
            rule-set trust-to-untrust {
                from zone IoT-Net1-trust;
                to zone untrust;
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                        /* Use egress interface source IP address */
                            interface;
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone IoT-Net1-trust to-zone IoT-Net2-trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone IoT-Net2-trust to-zone IoT-Net1-trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone untrust to-zone IoT-Net1-trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone untrust to-zone IoT-Net2-trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
        from-zone IoT-Net1-trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone IoT-Net2-trust to-zone untrust {
            policy internet-access {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
    }
    zones {
        security-zone IoT-Net1-trust {
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                            ssh;
                        }
                    }
                }
            }
        }    
        security-zone IoT-Net2-trust {
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                            ssh;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            interfaces {
                ge-0/0/0.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
    }
}
access {
    address-assignment {
        pool IoT-Net1 {
            family inet {
                network 192.168.11.0/28;
                range USERS {
                    low 192.168.11.2;
                    high 192.168.11.14;
                }
                dhcp-attributes {
                    maximum-lease-time 600; /* seconds */
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.11.1;
                    }
                }
            }
        }
        pool IoT-Net2 {
            family inet {
                network 192.168.12.0/26;
                range USERS {
                    low 192.168.12.2;
                    high 192.168.12.62;
                }
                dhcp-attributes {
                    maximum-lease-time 600; /* seconds */
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.12.1;
                    }
                }
            }
        }
    }
}
